import XCTest

import EmojiLogHandlerTests

var tests = [XCTestCaseEntry]()
tests += EmojiLogHandlerTests.allTests()
XCTMain(tests)
